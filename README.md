# Installing OpenWrt on Zyxel P-2812HNU-F3

# Table of Contents
- [Installing OpenWrt on Zyxel P-2812HNU-F3](#installing-openwrt-on-zyxel-p-2812hnu-f3)
- [Table of Contents](#table-of-contents)
- [Intro](#intro)
- [Resources](#resources)
- [Preparation](#preparation)
  - [Needed files](#needed-files)
  - [Compiling OpenWrt](#compiling-openwrt)
    - [Modifications](#modifications)
    - [Procedure for 21.02.1](#procedure-for-21021)
  - [Serial](#serial)
  - [TFTP](#tftp)
- [Process](#process)
  - [Replacing the factory U-Boot](#replacing-the-factory-u-boot)
    - [Procedure](#procedure)
      - [Tip for step 6](#tip-for-step-6)
    - [Shorting pads](#shorting-pads)
    - [Serial communication log: Procedure](#serial-communication-log-procedure)
    - [Serial communication log: Boot after successful replacement](#serial-communication-log-boot-after-successful-replacement)
  - [Installing OpenWrt](#installing-openwrt)
    - [Procedure](#procedure-1)
    - [Better WiFi performance](#better-wifi-performance)
    - [Serial communication log: Procedure](#serial-communication-log-procedure-1)

# Intro

This README contains my experience of getting OpenWrt installed on the Zyxel P-2812HNU-F3. I'm no professional, and this was a learning experience to me. I can therefore not guarantee everything here is good practice or the correct way of doing things, but hopefully it might help someone in doing this too :)

Initial install requires you to either use the images I have compiled myself, or you have to compile your own. For any future upgrades, you will need to compile your own, as official images don't work!

Installing requires using some "random" images that I was not able to find info about compiling myself, so take that however you want. 

# Resources
- [The OpenWrt device/wiki page for the P2812HNU-F3](https://openwrt.org/toh/zyxel/p-2812hnu-f3)
- [ScApis repo with (old) pre-built images and misc files](https://github.com/ScApi/P2812HNUFx-Pre-Build)
    - I used the files in the `OpenWrt-Designated-Driver-r47026` branch
- [OpenWrt build system usage](https://openwrt.org/docs/guide-developer/toolchain/use-buildsystem)
- Serial terminals
    - I used [CuteCom](https://gitlab.com/cutecom/cutecom) since it's great and I'm on Linux
    - The OpenWrt wiki recommends [Tera Term](https://ttssh2.osdn.jp/index.html.en) for Windows

# Preparation

## Needed files

From ScApis repo (OpenWrt-Designated-Driver-r47026 branch):
- `openwrt-lantiq-p2812hnufx_ram-u-boot.asc`
- `openwrt-lantiq-p2812hnufx_nor-u-boot.img`
- `openwrt-lantiq-xrx200-P2812HNUF3-uImage`
- `RT3092.eeprom`

From this repo:
- `openwrt-21.02.1-lantiq-xrx200-zyxel_p-2812hnu-f3-initramfs-kernel_with_fix.bin`
- `openwrt-21.02.1-lantiq-xrx200-zyxel_p-2812hnu-f3-squashfs-sysupgrade_with_fix.bin`

## Compiling OpenWrt
Optional for install, required for updates

---

### Modifications

To be able to install OpenWrt, modifications have to be made to the flash layout. This is because newer kernels are larger than the 2MB that is allocated by default. 

File: `target/linux/lantiq/files/arch/mips/boot/dts/lantiq/vr9_zyxel_p-2812hnu-f3.dts`

Diff:
```
@@ -53,11 +53,11 @@
 
                partition@0 {
                        label = "kernel";
-                       reg = <0x0 0x200000>;
+                       reg = <0x0 0x300000>;
                };
                partition@200000 {
                        label = "ubi";
-                       reg = <0x200000 0x7e00000>;
+                       reg = <0x300000 0x7d00000>;
                };
        };
 }; 
```

### Procedure for 21.02.1

Take a look at the [OpenWrt build system usage](https://openwrt.org/docs/guide-developer/toolchain/use-buildsystem) page for more details. 

---

Clone and checkout sources
``` 
git clone https://git.openwrt.org/openwrt/openwrt.git
cd openwrt
git checkout v21.02.1
```

After this step, do the changes shown in the section above.

Then continue:

```
./scripts/feeds update -a
./scripts/feeds install -a
```

Optionally grab the official config
```
wget https://downloads.openwrt.org/releases/21.02.1/targets/lantiq/xrx200/config.buildinfo -O .config
```

For the last step, you need to manually select device. 
Make sure these settings are set:
- Target System: `Lantiq`
- Subtarget: `XRX200`
- Target Profile `ZyXEL P-2812HNU F3`

Choose `Exit`, then `Yes` to save.
```
make menuconfig
```

Then start building!
```
make -j $(nproc) defconfig download clean world
```

Images will be put in `bin/targets/lantiq/xrx200/`

## Serial

A serial connection to the router is required for this process. Pinout for the board connector can be found on the wiki page. I used a small serial to usb adapter that can be found cheaply online. 

Remember: 
- Rx on the board goes to Tx on your computer, Tx on the board goes to Rx on your computer!
- **Do not connect the 3.3v pin. It may break the board!**

![The P2812HNU-F3 mainboard with a serial to usb adapter connected to it with jumper wires](/images/serial-connection.jpg)

## TFTP

For TFTP I used [this guide](https://fedoramagazine.org/how-to-set-up-a-tftp-server-on-fedora/), which is aimed at Fedora users. While it does work, please read the comment by Edgar Hoch before following it, as the guide is overcomplicates it for this usecase and not everything in it is necessary.

# Process

## Replacing the factory U-Boot

### Procedure

Replacing the factory U-Boot is fairly straight forward. Just follow the steps described on the [OpenWrt Wiki](https://openwrt.org/toh/zyxel/p-2812hnu-f3#replacement_of_factory_u-boot). This part uses the images from ScApis GitHub repo. 

#### Tip for step 6

A quick one-liner for getting the image size in hex format ([source](https://askubuntu.com/a/831692)):

```Bash
printf "%x\n" `stat -c "%s" {filename}`
```


The filesize should also be displayed on the serial connection after step 5.

### Shorting pads

The hardest part here, for me at least, was shorting the R24 and R15 pads. They are found close to the serial pins on the board. 

![A close-up picture of the P2812HNU-F3 mainboard with red circles drawn around pads R24 and R15](/images/pins.jpg)

I ended up masking around the pads with electrical tape, as to not break anything by poking around with screwdrivers to try shorting the pads. 

![A close-up picture of the P2812HNU-F3 mainboard where the area around pads R24 and R15 is covered in electrical tape](/images/mask.jpg)

When you get the pads shorted right, power on the router and you should see something like this on the serial connection:
```
ROM VER: 1.0.5
CFG 04
UART
```

The important part here is the `CFG 04` mode

### Serial communication log: Procedure

```
P-2812HNU-Fx # protect off all
Un-Protect Flash Bank # 1
....................................................................................................................................... done
P-2812HNU-Fx # erase all
Erase Flash Bank # 1 
....................................................................................................................................... done
P-2812HNU-Fx # tftp 0x80700000 openwrt-lantiq-p2812hnufx_nor-u-boot.img
ltq_phy: addr 0, link 1, speed 1000, duplex 1
ltq_phy: addr 1, link 0, speed 10, duplex 0
ltq_phy: addr 17, link 0, speed 10, duplex 0
ltq_phy: addr 19, link 0, speed 10, duplex 0
ltq_phy: addr 5, link 0, speed 10, duplex 0
Using ltq-eth device
TFTP from server 192.168.1.2; our IP address is 192.168.1.1
Filename 'openwrt-lantiq-p2812hnufx_nor-u-boot.img'.
Load address: 0x80700000
Loading: *<0x08>####################################
	 3 MiB/s
done
Bytes transferred = 521300 (7f454 hex)
P-2812HNU-Fx # cp.b 0x80700000 0xb0000000 0x7f454
Copy to Flash... done
P-2812HNU-Fx #
```

### Serial communication log: Boot after successful replacement
On the serial connection, you should see something like this if you got it right:
```
ROM VER: 1

U-Boot 2013.10-openwrt5 (Apr 24 2018 - 18:56:42) P-2812HNU-Fx

Board: ZyXEL P-2812HNU-Fx
SoC:   Lantiq VRX288 v1.1
CPU:   500 MHz
IO:    250 MHz
BUS:   250 MHz
BOOT:  NOR
DRAM:  128 MiB
Flash: 8 MiB
NAND:  128 MiB
```


## Installing OpenWrt

**You must have replaced the factory U-Boot for this to work!**

---

### Procedure

With the new U-Boot, stop the boot process on the serial connection before it starts booting. 

Then follow these steps, which differ a bit from the steps in the wiki, with self-compiled images or the ones from this repo:

```
setenv nboot 'nand read 0x80800000 0x0 0x300000; bootm 0x80800000'
setenv bootcmd 'run nboot'
saveenv

tftp 0x80800000 openwrt-lantiq-xrx200-P2812HNUF3-uImage

nand erase.chip 

nand write 0x80800000 0x0 0x300000 

tftpboot 0x80800000 openwrt-21.02.1-lantiq-xrx200-zyxel_p-2812hnu-f3-initramfs-kernel_with_fix.bin

bootm 0x80800000
```

The router should now boot.

Perform a normal upgrade from the web interface with the `openwrt-21.02.1-lantiq-xrx200-zyxel_p-2812hnu-f3-squashfs-sysupgrade_with_fix.bin` image, and you should be good to go! :)

I unchecked `Keep settings and retain the current configuration`, but I'm not sure it matters.

**If you upgrade with an official image now or in the future, the device will not boot!!**


### Better WiFi performance

After finished install, the OpenWrt wiki says to put the `RT3092.eeprom` file in `/lib/firmware` for better WiFi performance. I did not notice a difference in speeds from doing it, but is probably worth doing anyway. Might be because my internet speed isn't enough to cap it anyway. Have not tested speeds to a local device. 

### Serial communication log: Procedure

```
ROM VER: 1

U-Boot 2013.10-openwrt5 (Apr 24 2018 - 18:56:42) P-2812HNU-Fx

Board: ZyXEL P-2812HNU-Fx
SoC:   Lantiq VRX288 v1.1
CPU:   500 MHz
IO:    250 MHz
BUS:   250 MHz
BOOT:  NOR
DRAM:  128 MiB
Flash: 8 MiB
NAND:  128 MiB
In:    serial
Out:   serial
Err:   serial
Net:   ltq-eth
Hit any key to stop autoboot:  2  
P-2812HNU-Fx # setenv nboot 'nand read 0x80800000 0x0 0x300000; bootm 0x80800000'
P-2812HNU-Fx # setenv bootcmd 'run nboot'
P-2812HNU-Fx # saveenv
Saving Environment to Flash...
. done
Un-Protected 1 sectors
Erasing Flash...
. done
Erased 1 sectors
Writing to Flash... done
. done
Protected 1 sectors
P-2812HNU-Fx # tftp 0x80800000 openwrt-lantiq-xrx200-P2812HNUF3-uImage
ltq_phy: addr 0, link 1, speed 1000, duplex 1
ltq_phy: addr 1, link 0, speed 10, duplex 0
ltq_phy: addr 17, link 0, speed 10, duplex 0
ltq_phy: addr 19, link 0, speed 10, duplex 0
ltq_phy: addr 5, link 0, speed 10, duplex 0
Using ltq-eth device
TFTP from server 192.168.1.2; our IP address is 192.168.1.1
Filename 'openwrt-lantiq-xrx200-P2812HNUF3-uImage'.
Load address: 0x80800000
Loading: *<0x08>#################################################################
	 #####################################################
	 3.5 MiB/s
done
Bytes transferred = 1724373 (1a4fd5 hex)
P-2812HNU-Fx # nand erase.chip

NAND erase.chip: device 0 whole chip
Erasing at 0x0 --   0% complete.Erasing at 0x140000 --   1% complete.Erasing at 0x280000 --   2% complete.Erasing at 0x3c0000 --   3% complete.Erasing at 0x500000 --   4% complete.Erasing at 0x660000 --   5% complete.Erasing at 0x7a0000 --   6% complete.Erasing at 0x8e0000 --   7% complete.Erasing at 0xa20000 --   8% complete.Erasing at 0xb80000 --   9% complete.Erasing at 0xcc0000 --  10% complete.Erasing at 0xe00000 --  11% complete.Erasing at 0xf40000 --  12% complete.Erasing at 0x10a0000 --  13% complete.Erasing at 0x11e0000 --  14% complete.Erasing at 0x1320000 --  15% complete.Erasing at 0x1460000 --  16% complete.Skipping bad block at  0x01500000                                          
Erasing at 0x15c0000 --  17% complete.Erasing at 0x1700000 --  18% complete.Erasing at 0x1840000 --  19% complete.Skipping bad block at  0x01920000                                          
Erasing at 0x1980000 --  20% complete.Erasing at 0x1ae0000 --  21% complete.Erasing at 0x1c20000 --  22% complete.Erasing at 0x1d60000 --  23% complete.Erasing at 0x1ea0000 --  24% complete.Erasing at 0x1fe0000 --  25% complete.Erasing at 0x2140000 --  26% complete.Erasing at 0x2280000 --  27% complete.Erasing at 0x23c0000 --  28% complete.Erasing at 0x2500000 --  29% complete.Erasing at 0x2660000 --  30% complete.Erasing at 0x27a0000 --  31% complete.Erasing at 0x28e0000 --  32% complete.Erasing at 0x2a20000 --  33% complete.Erasing at 0x2b80000 --  34% complete.Erasing at 0x2cc0000 --  35% complete.Erasing at 0x2e00000 --  36% complete.Erasing at 0x2f40000 --  37% complete.Erasing at 0x30a0000 --  38% complete.Erasing at 0x31e0000 --  39% complete.Erasing at 0x3320000 --  40% complete.Erasing at 0x3460000 --  41% complete.Erasing at 0x35c0000 --  42% complete.Erasing at 0x3700000 --  43% complete.Erasing at 0x3840000 --  44% complete.Erasing at 0x3980000 --  45% complete.Erasing at 0x3ae0000 --  46% complete.Erasing at 0x3c20000 --  47% complete.Erasing at 0x3d60000 --  48% complete.Erasing at 0x3ea0000 --  49% complete.Erasing at 0x3fe0000 --  50% complete.Erasing at 0x4140000 --  51% complete.Erasing at 0x4280000 --  52% complete.Erasing at 0x43c0000 --  53% complete.Erasing at 0x4500000 --  54% complete.Erasing at 0x4660000 --  55% complete.Erasing at 0x47a0000 --  56% complete.Erasing at 0x48e0000 --  57% complete.Erasing at 0x4a20000 --  58% complete.Erasing at 0x4b80000 --  59% complete.Erasing at 0x4cc0000 --  60% complete.Erasing at 0x4e00000 --  61% complete.Erasing at 0x4f40000 --  62% complete.Erasing at 0x50a0000 --  63% complete.Erasing at 0x51e0000 --  64% complete.Erasing at 0x5320000 --  65% complete.Erasing at 0x5460000 --  66% complete.Erasing at 0x55c0000 --  67% complete.Erasing at 0x5700000 --  68% complete.Erasing at 0x5840000 --  69% complete.Erasing at 0x5980000 --  70% complete.Erasing at 0x5ae0000 --  71% complete.Erasing at 0x5c20000 --  72% complete.Erasing at 0x5d60000 --  73% complete.Erasing at 0x5ea0000 --  74% complete.Erasing at 0x5fe0000 --  75% complete.Erasing at 0x6140000 --  76% complete.Erasing at 0x6280000 --  77% complete.Erasing at 0x63c0000 --  78% complete.Erasing at 0x6500000 --  79% complete.Erasing at 0x6660000 --  80% complete.Erasing at 0x67a0000 --  81% complete.Erasing at 0x68e0000 --  82% complete.Erasing at 0x6a20000 --  83% complete.Erasing at 0x6b80000 --  84% complete.Erasing at 0x6cc0000 --  85% complete.Erasing at 0x6e00000 --  86% complete.Erasing at 0x6f40000 --  87% complete.Erasing at 0x70a0000 --  88% complete.Erasing at 0x71e0000 --  89% complete.Erasing at 0x7320000 --  90% complete.Erasing at 0x7460000 --  91% complete.Erasing at 0x75c0000 --  92% complete.Erasing at 0x7700000 --  93% complete.Erasing at 0x7840000 --  94% complete.Skipping bad block at  0x07940000                                          
Erasing at 0x7980000 --  95% complete.Erasing at 0x7ae0000 --  96% complete.Erasing at 0x7c20000 --  97% complete.Erasing at 0x7d60000 --  98% complete.Erasing at 0x7ea0000 --  99% complete.Erasing at 0x7fe0000 -- 100% complete.
OK
P-2812HNU-Fx # nand write 0x80800000 0x0 0x300000

NAND write: device 0 offset 0x0, size 0x300000
 3145728 bytes written: OK
P-2812HNU-Fx # tftpboot 0x80800000 openwrt-21.02.1-lantiq-xrx200-zyxel_p-2812hnu-f3-initramfs-kernel_with_fix.bin
ltq_phy: addr 0, link 1, speed 1000, duplex 1
ltq_phy: addr 1, link 0, speed 10, duplex 0
ltq_phy: addr 17, link 0, speed 10, duplex 0
ltq_phy: addr 19, link 0, speed 10, duplex 0
ltq_phy: addr 5, link 0, speed 10, duplex 0
Using ltq-eth device
TFTP from server 192.168.1.2; our IP address is 192.168.1.1
Filename 'openwrt-21.02.1-lantiq-xrx200-zyxel_p-2812hnu-f3-initramfs-kernel_with_fix.bin'.
Load address: 0x80800000
Loading: *<0x08>#################################################################
	 #################################################################
	 #################################################################
	 #################################################################
	 #################################################################
	 #################################################################
	 ################################################
done
Bytes transferred = 6417286 (61eb86 hex)
P-2812HNU-Fx # bootm 0x80800000
## Booting kernel from Legacy Image at 80800000 ...
   Image Name:   MIPS OpenWrt Linux-5.4.154
   Created:      2021-10-24   9:01:35 UTC
   Image Type:   MIPS Linux Kernel Image (lzma compressed)
   Data Size:    6417222 Bytes = 6.1 MiB
   Load Address: 80002000
   Entry Point:  80002000
   Verifying Checksum ... OK
   Uncompressing Kernel Image ... OK

Starting kernel ...

[    0.000000] Linux version 5.4.154 (builder@buildhost) (gcc version 8.4.0 (OpenWrt GCC 8.4.0 r16325-88151b8303)) #0 SMP Sun Oct 24 09:01:35 2021

...

```
